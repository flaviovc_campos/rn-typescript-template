
# Template React Native - TypeScript

Template base para iniciar projetos em React Native(Bare Workflow - react-native-cli).
Pré configurado com uma estrutura simples de refrerência e Libs auxiliares.

****

## Pré Requisitos de Ambiente

- [Referencia React Native](https://reactnative.dev/docs/environment-setup)
- [React Native CLI](https://reactnative.dev/docs/environment-setup)
- [NodeJs](https://nodejs.org/en/download/)
- [Java -JDK](https://www.oracle.com/java/technologies/downloads/)
- [Android Studio](https://developer.android.com/studio/install)

***

## Criar novo Projeto

- Criar o projeto referenciando o template

```bash
  npx react-native init MyApp --template <Git Repo>
  cd MyApp
```

- O template inicia configurado com o nome padrâo. É necessario
  reconfigurar a estrutura do Projeto com o nome desejado.

```bash
  npm install -g react-native-rename
  npx react-native-rename MyApp -b com.company.myapp
```

- Instalar as dependências do package.json

```bash
  npm install
```

***

### Debug
Scripts em `package.json`

- Android

```bash
  npm run android //Carrega variaveis - .env
  npm run android:dev //Carrega variaveis - .env.dev
  npm run android:stage //Carrega variaveis - .env.stage
  npm run android:prod //Carrega variaveis - .env
```

- iOS - Necessário equipamentto Apple(Mac Mini, iMac, MacBook, iPhone, iPad)

```bash
  npm run ios //Carrega variaveis - .env
  npm run ios:dev //Carrega variaveis - .env.dev
  npm run ios:stage //Carrega variaveis - .env.stage
  npm run ios:prod //Carrega variaveis - .env
```

***

## Bibliotecas/Frameworks auxiliares

- [TypeScript](https://reactnative.dev/docs/typescript#getting-started-with-typescript)
- [React Navigation](https://reactnavigation.org/docs/hello-react-navigation)
- [Enviroments](https://github.com/luggit/react-native-config)
- [Redux Toolkit](https://redux-toolkit.js.org/tutorials/overview)
- [Redux DevTools](https://github.com/reduxjs/redux-devtools)
- [ThemeProvider](https://github.com/callstack/react-theme-provider)
- [Log](https://github.com/onubo/react-native-logs)
- [AsyncStorage](https://react-native-async-storage.github.io/async-storage/docs/install/)
- [Axios](https://github.com/axios/axios)
