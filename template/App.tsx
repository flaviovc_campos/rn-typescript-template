import React from 'react';
import StackNavigator from "@config/navigation/StackNavigator";
import { Provider } from 'react-redux'
import store from "repository/state/store";

const App = () => {

    return (
        <Provider store={store}>
            <StackNavigator/>
        </Provider>
    )
}

export default App
