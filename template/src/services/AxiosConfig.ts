import axios from "axios";
import Config from "react-native-config";

const AxiosInstance = axios.create({
    baseURL: Config.BASE_URL,
    timeout: 60000,
    responseType: "json",
    headers: {"content-type": "application/json"},
})

AxiosInstance.interceptors.request.use(async (config) => {
    //const token = await getToken() //AsyncStorage

    /*if (token && config.headers) {
        config.headers.Authorization = `Bearer ${token}`;
    }*/

    return config;
})

AxiosInstance.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject(error)
)

export default AxiosInstance
