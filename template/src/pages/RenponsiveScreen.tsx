import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {responsiveFontSize, responsiveHeight, responsiveWidth} from "utils/Dimensions";

const RenponsiveScreen = () =>{

    return(
        <>
            <View style={styles.container1}/>
            <View style={styles.container2}/>

            <>
                <Text style={styles.font1}>Exemplo 1</Text>
                <Text style={styles.font2}>Exemplo 2</Text>
                <Text style={styles.font3}>Exemplo 3</Text>
            </>
        </>
    )

}

const styles = StyleSheet.create({
    container1:{
        width: responsiveWidth(50),
        height: responsiveHeight(20),
        backgroundColor: 'blue',
        marginVertical: 5
    },
    container2:{
        width: responsiveWidth(80),
        height: responsiveHeight(30),
        backgroundColor: 'red',
        marginVertical: 5
    },
    font1:{
        fontSize: responsiveFontSize(2)
    },
    font2:{
        fontSize: responsiveFontSize(3)
    },
    font3:{
        fontSize: responsiveFontSize(4)
    }
})

export default RenponsiveScreen
