import React from "react";
import {StyleSheet, Text, View} from "react-native";
import ButtonExample from "components/ButtonExample";
import {useAppDispatch, useAppSelector} from "repository/state/hooks";
import {decrementValue, incrementValue, incrementValueByAmount} from "repository/state/exampleSlice";
import {useTheme} from "config/theme/ThemeCustomProvider";

const ReduxScreen = () => {

    const theme = useTheme()

    //Redux and Hooks
    const dispatch = useAppDispatch()
    const {exampleValue} = useAppSelector((state) => state.example)
    //OR
    //const exampleValue2 = useAppSelector((state)=> state.example.exampleValue)
    //OR
    //const exampleValue3 = getExampleValueState


    const incrementExampleStateValue = () => {
        dispatch(incrementValue())
    }

    const decrementExampleStateValue = () => {
        dispatch(decrementValue())
    }

    const incrementByAmountExampleStateValue = () => {
        dispatch(incrementValueByAmount(5))
    }

    return (
        <View style={[styles.container, {backgroundColor: theme.background}]}>

            <ButtonExample text={'Increment ExampleValue State'} onPress={incrementExampleStateValue}/>
            <ButtonExample text={'Decrement ExampleValue State'} onPress={decrementExampleStateValue}/>
            <ButtonExample text={'Increment 5 ExampleValue State'} onPress={incrementByAmountExampleStateValue}/>

            <Text style={styles.text}>{exampleValue}</Text>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        alignItems: 'center'
    },
    text:{
        marginTop: 30,
        fontSize: 25
    }
})

export default ReduxScreen
