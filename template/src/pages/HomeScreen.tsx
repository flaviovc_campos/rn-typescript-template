import React, {useEffect} from "react";
import {StyleSheet, View} from "react-native";
import {useNavigation} from "@react-navigation/native";
import ButtonExample from "components/ButtonExample";
import {NativeStackNavigationProp} from "@react-navigation/native-stack";
import {RootStackParamList} from "config/navigation/StackNavigator";
import {useTheme} from "config/theme/ThemeCustomProvider";
import Log from "config/logging/Log";
import {getJSONFake} from "services/Methods";
import Config from "react-native-config";

type HomeNavigationProp = NativeStackNavigationProp<RootStackParamList, 'Home'>

const HomeScreen = () => {

    const navigation = useNavigation<HomeNavigationProp>()
    const theme = useTheme()

    const goToReduxScreen = () => navigation.navigate("Redux")
    const goToResponsiveScreen = () => navigation.navigate("Responsive")

    useEffect(()=>{
        Log.info('INFO ->', 'OK')
        Log.success('SUCCESS ->', 'OK')
        Log.warn('WARN ->', 'OK')
        Log.error('ERROR ->', 'OK')

        getJsonFake()

    },[])

    const getJsonFake = async() =>{
        const jsonFake = await getJSONFake()
        if(jsonFake){
            Log.info('ENVIROMENT ->', Config.ENV)
            Log.success('JSON FAKE ->', jsonFake.data)
        }
    }

    return (
        <View style={[styles.container, {backgroundColor: theme.background}]}>

            <ButtonExample text={'Redux Example'} onPress={goToReduxScreen}/>
            <ButtonExample text={'Responsive Example'} onPress={goToResponsiveScreen}/>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default HomeScreen
