import React, {FC} from "react";
import {TouchableOpacity, Text, StyleSheet} from "react-native";
import {useTheme} from "config/theme/ThemeCustomProvider";

export type ButtonProps = {
    text: string;
    onPress: () => void
}

const ButtonExample = ({text, onPress}: ButtonProps) =>{

    const theme = useTheme()

    return(
        <TouchableOpacity onPress={onPress} style={[styles.button, {backgroundColor: theme.secondary}]}>
            <Text>{text}</Text>
        </TouchableOpacity>
    )
}

const  styles = StyleSheet.create({
    button:{
        width: '100%',
        paddingVertical: 10,
        margin: 10,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default ButtonExample
