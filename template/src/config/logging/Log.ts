import {logger, consoleTransport} from "react-native-logs"

const defaultConfig = {
    levels: {
        info: 0,
        success: 1,
        warn: 2,
        error: 3,
    },
    severity: "debug",
    transport: consoleTransport,
    transportOptions: {
        colors: {
            info: "cyanBright",
            success: "greenBright",
            warn: "yellowBright",
            error: "redBright",
        },
    },
    async: true,
    dateFormat: "local",
    printLevel: false,
    printDate: true,
    enabled: __DEV__,
}

const Log = logger.createLogger(defaultConfig)

export default Log
