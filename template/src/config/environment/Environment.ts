import {BASE_URL_DEV, BASE_URL_PROD, BASE_URL_STAGE} from "@env";
import config from 'react-native-config';

const ENV = {
    dev: {
        api_base_url: BASE_URL_DEV
    },
    stage: {
        api_base_url: BASE_URL_STAGE
    },
    prod: {
        api_base_url: BASE_URL_PROD
    }
}
