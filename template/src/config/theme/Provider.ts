import { Theme } from "./theme";
import { Platform, Dimensions } from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;

export type { Theme };

export const isIphoneX = platform === "ios" &&
    (deviceHeight === 812 || deviceWidth === 812 || deviceHeight === 844 ||
        deviceWidth === 844 || deviceHeight === 896 || deviceWidth === 896 ||
        deviceHeight === 926 || deviceWidth === 926);
