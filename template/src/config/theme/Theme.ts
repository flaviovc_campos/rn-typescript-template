export const lightTheme = {
    dark: false,
    colors: {
        primary: "#FFFFFF",
        secondary: "#F1F1F1"
    },
};

export const darkTheme = {
    dark: true,
    colors: {
        primary: "#FFFFFF",
        secondary: "#F1F1F1"
    },
}

export interface Theme {
    dark: boolean
    colors: {
        primary: string;
        secondary: string;
    }
}
