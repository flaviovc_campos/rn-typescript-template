import {createTheming} from "@callstack/react-theme-provider";

const lightTheme = {
    primary: "#8c8c8c",
    secondary: "#e5e5e5",
    background: "#FFFFFF",
    card: "#F1F1F1",
    text: "#8c8c8c",
    border: "#F1F1F1",
    notification: "#F1F1F1"
}

const { ThemeProvider, withTheme, useTheme } = createTheming(lightTheme)

export {ThemeProvider, withTheme, useTheme}
