declare module '@dimensions' {
    export function rw(w: number): number;
    export function rh(h: number): number;
    export function rf(f: number): number;
}
