import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from "pages/HomeScreen";
import {ThemeProvider} from "config/theme/ThemeCustomProvider";
import RenponsiveScreen from "pages/RenponsiveScreen";
import ReduxScreen from "pages/ReduxScreen";

declare global {
    namespace ReactNavigation {
        interface RootParamsList extends RootStackParamList {
        }
    }
}

export type RootStackParamList = {
    Home: undefined;
    Redux: undefined;
    Page2: undefined;
    Responsive: undefined;
}

const AppStack = createNativeStackNavigator<RootStackParamList>()

export default function StackNavigator() {

    return (
        <ThemeProvider>
            <NavigationContainer>
                <AppStack.Navigator>
                    <AppStack.Screen name='Home' component={HomeScreen} options={{headerShown: false}}/>
                    <AppStack.Screen name='Redux' component={ReduxScreen} />
                    <AppStack.Screen name='Responsive' component={RenponsiveScreen} />
                </AppStack.Navigator>
            </NavigationContainer>
        </ThemeProvider>
    )
}
