import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import {RootState} from "repository/state/store";

export interface ExampleState {
    exampleValue: number
}

const initialState: ExampleState = {
    exampleValue: 0,
}

//Slice
export const exampleSlice = createSlice({
    name: 'example',
    initialState,
    reducers:{
        incrementValue: (state: ExampleState) =>{ state.exampleValue += 1},
        decrementValue: (state: ExampleState) =>{ state.exampleValue -= 1},
        incrementValueByAmount: (state:ExampleState, action: PayloadAction<number>) =>{ state.exampleValue += action.payload}
    }
})
export default exampleSlice.reducer

//Action
export const { incrementValue, decrementValue, incrementValueByAmount } = exampleSlice.actions

export const getExampleValueState = (state: RootState) => state.example.exampleValue
