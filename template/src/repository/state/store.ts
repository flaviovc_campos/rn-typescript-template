import { configureStore } from '@reduxjs/toolkit'
import {exampleSlice} from "repository/state/exampleSlice";


const store = configureStore({
    reducer: {
        example: exampleSlice.reducer
    },
})

// Infer the `RootState` and `AppDispatch` types from the state itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch


export default store
