import AsyncStorage from "@react-native-async-storage/async-storage";
import {StorageKeys} from "repository/storage/StorageKeys";
import Log from "config/logging/Log";

export const setIsLogged = async (isLogged: string) =>{
    try{
        //Async Storage can only store STRING data!!!
        await AsyncStorage.setItem(StorageKeys.IS_LOGGED, isLogged)
    }catch(e){
        Log.error('Error setIsLogged', e)
    }
}

export const getIsLogged = async () =>{
    try{
        const isLogged = await AsyncStorage.getItem(StorageKeys.IS_LOGGED) || "false"
        return isLogged
    }catch(e){
        Log.error('Error getIsLogged', e)
    }
}

export const setUserInfo = async(userInfo: {name: string, age: number}) =>{
    try{
        const jsonValue = JSON.stringify(userInfo)
        await AsyncStorage.setItem(StorageKeys.USER_INFO, jsonValue)
    }catch(e){
        Log.error('Error setUserInfo', e)
    }
}

export const getUserInfo = async() =>{
    try {
        const userInfo = await AsyncStorage.getItem(StorageKeys.USER_INFO)
        return userInfo != null ? JSON.parse(userInfo) : null
    }catch (e) {
        Log.error('Error getUserInfo', e)
    }
}
