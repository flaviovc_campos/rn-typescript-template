import { Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");
const sqrt = Math.sqrt(Math.pow((16 / 9) * width, 2) + Math.pow(width, 2));


const getOrientation = () =>{
    return width < height ? 'portrait' : 'landscape'
}

export const responsiveHeight = (h: number): number => {
    const _height = getOrientation() == 'portrait' ? height : width
    return Math.round(_height * (h / 100));
}

export const responsiveWidth = (w: number): number => {
    const _width = getOrientation() == 'portrait' ? width : height
    return Math.round(_width * (w / 100));
}

export const responsiveFontSize = (f: number): number => {
    return Math.round(sqrt * (f / 100));
}
