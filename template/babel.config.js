module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      "module-resolver",
      {
        root: ["./src"],
        extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json", ".d.ts", "d.tsx"],
        alias: {
          "@assets": "./src/assets",
          "@components": "./src/components",
          "@models": "./src/models",
          "@pages": "./src/pages",
          "@services": "./src/services",
          "@repository": "./src/repository",
          "@utils": "./src/utils",
          "@config": "./src/config",
          "@dimentions": "./src/config/dimensions",
          "@navigation": "./src/config/navigation"
        },
      },
    ],
  ],
};
